# irc-sbc-buildroot
This is a fork of buildroot to provide support for building images for the
Orange Pi Zero.

The `master` branch of this repository holds the full project-specific
configuration for building an IRC-SBC image. Basic support for the Orange Pi
Zero board is kept in `base`, please refer to that branch if you'd like to
make a minimal system image or develop your own customised image.

## Building an image
Run the following commands:

    make orangepi_zero_defconfig
    make

After a while (quite a while if this is your first run) your image will be
ready in `output/images/sdcard.img`, which you can write to a microSD card
using `dd`. Assuming your card is accessible at /dev/mmcblk0:

    dd if=output/images/sdcard.img of=/dev/mmcblk0 bs=1M

That's it, you're done. You can now boot off the card with your Orange Pi Zero.
