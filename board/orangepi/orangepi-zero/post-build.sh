#!/bin/sh
# post-build.sh for OrangePi Zero taken from generic OrangePi's post-build.sh

BOARD_DIR="$(dirname $0)"
CURRENT_DIR=`pwd`
XRADIO_DIR=`pwd`/$BOARD_DIR/wireless/xradio
MKIMAGE=$HOST_DIR/usr/bin/mkimage
BOOT_CMD=$BOARD_DIR/boot.cmd
BOOT_CMD_H=$BINARIES_DIR/boot.scr

# Compile wireless drivers and add firmware to FS
cd $XRADIO_DIR
cd $XRADIO_DIR && make ARCH=arm CROSS_COMPILE=$HOST_DIR/usr/bin/arm-buildroot-linux-uclibcgnueabihf- -C $BUILD_DIR/linux-orangepizero M=$XRADIO_DIR modules
cd $XRADIO_DIR && make ARCH=arm CROSS_COMPILE=$HOST_DIR/usr/bin/arm-buildroot-linux-uclibcgnueabihf- -C $BUILD_DIR/linux-orangepizero M=$XRADIO_DIR INSTALL_MOD_PATH=$TARGET_DIR modules_install
cd $CURRENT_DIR

mkdir -p $TARGET_DIR/lib/firmware/xr819
cp $BOARD_DIR/wireless/firmware/*.bin $TARGET_DIR/lib/firmware/xr819/
if ! grep -q xradio $TARGET_DIR/etc/inittab
then
    echo "Added xradio"
    #echo "::once:modprobe xradio_wlan" >> $TARGET_DIR/etc/inittab
fi

# U-Boot script
$MKIMAGE -C none -A arm -T script -d $BOOT_CMD $BOOT_CMD_H

